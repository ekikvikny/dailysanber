import React from 'react';
import {} from 'react-navigation';
import { View, Text,ScrollView, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StatusBar } from 'expo-status-bar';




export default class App extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <StatusBar
                    backgroundColor={"white"}
                    translucent={false} />

                <View style={styles.title}><Text style={{ fontWeight: 'bold', fontSize: 36, color: '#003366' }}>Tentang Saya</Text></View>
                <View style={styles.content}>
                    <Icon style={styles.foto} name="account-circle" size={200} />
                    <Text style={styles.nama}>Ekik VIkny</Text>
                    <Text style={styles.jabatan}>React Native Developer</Text>
                    <View style={styles.box}>
                        <Text style={styles.isiKonten}>Portofolio</Text>
                        <Text style={styles.borders}></Text>
                        <View style={styles.page}>
                            <View style={styles.konten}>
                                <Icon name="gitlab" style={styles.Ikon} size={50} />
                                <Text style={styles.isiKonten}>@ekikvikny</Text>
                            </View>
                            <View style={styles.konten}>
                                <Icon name="github-circle" style={styles.Ikon} size={50} />
                                <Text style={styles.isiKonten}>@ekkvikny</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.box2}>
                        <Text style={styles.isiKonten}>Hubungi Saya</Text>
                        <Text style={styles.borders}></Text>
                        <View style={styles.page}>
                            <View style={styles.konten}>
                                <Icon name="facebook-box" style={styles.Ikon} size={50} />
                                <Text style={styles.isiKonten}>@ekikvikny</Text>
                            </View>
                            <View style={styles.konten}>
                                <Icon name="instagram" style={styles.Ikon} size={50} />
                                <Text style={styles.isiKonten}>@ekikvikny</Text>
                            </View>
                            <View style={styles.konten}>
                                <Icon name="twitter" style={styles.Ikon} size={50} />
                                <Text style={styles.isiKonten}>@ekikvikny</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>



        )
    }
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    borders: {
        borderWidth: 1,
        width: 360,
        borderColor: '#003366',
        marginTop: 10,
        height: 1
    },

    title: {
        marginTop: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 55
    },
    content: {
        height: 600,
        alignItems: 'center',
        padding: 20
    },
    box: {
        marginTop: 20,
        backgroundColor: '#EFEFEF',
        height: 180,
        width: 360,
        borderRadius: 10,

    },
    box2: {
        marginTop: 20,
        backgroundColor: '#EFEFEF',
        height: 180,
        width: 360,
        borderRadius: 10,

    },
    foto: {
        textAlign: 'center',
        height: 200,
        width: 200,
        color: '#e3e3e3'
    },
    nama: {
        textAlign: 'center',
        marginTop: 10,
        color: '#003366',
        fontSize: 24,
        fontWeight: 'bold'
    },
    isiKonten: {
        textAlign: 'left',
        marginTop: 10,
        marginLeft: 10,
        color: '#003366',
        fontSize: 18
    },
    jabatan: {
        textAlign: 'center',
        marginTop: 10,
        marginLeft: 10,
        color: '#3EC6FF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    page: {
        width: 360,
        height: 150,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    konten: {
        justifyContent: 'center', 
        alignItems: 'center',
        flex:1
    },
    Ikon: {
        padding: 5,
        textAlign: 'center',
        color: '#3EC6FF'
    }

});
