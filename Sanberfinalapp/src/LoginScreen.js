import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, StatusBar,Image,TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import * as firebase from 'firebase';
import { FormLabel, FormInput } from 'react-native-elements';


var firebaseConfig = {
  apiKey: "AIzaSyCgDGoZTG85htpFQhVzBj5NzaVvddshuh4",
  authDomain: "sanberdaily.firebaseapp.com",
  databaseURL: "https://sanberdaily.firebaseio.com",
  projectId: "sanberdaily",
  storageBucket: "sanberdaily.appspot.com",
  messagingSenderId: "78564833394",
  appId: "1:78564833394:web:c96e3490a9d964f84faaeb",
  measurementId: "G-3E5RCE2MFW"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      isError: false,
      loading:false
    }
  }

  loginHandler() {
    console.log(this.state.email, ' ', this.state.password)
    this.setState({error:'', loading:true});
        
        const{email, password} = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {
            this.setState({error:'', loading:false});
            this.props.navigation.navigate("Home", {
              email: this.state.email
           })
        })
        .catch(() => {
            this.setState({error:'Authentication Failed', loading:false});
            this.state.isError = true;
        })
  }

  onSignUpPress(){
      this.setState({error:'', loading:true});
      
      const{email, password} = this.state;
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(() => {
          this.setState({error:'', loading:false});
          this.props.navigation.navigate("Home", {
            email: this.state.email
         });
      })
      .catch(() => {
          this.setState({error:'Authentication Failed', loading:false});
          this.state.isError = true;
      })

  }

  render() {
    return (
    <View style={styles.container}>
      <StatusBar
          backgroundColor={"white"}
          translucent={false} />
      <View style={styles.header}>
          <Image source={require('./images/logo.png')} style={styles.logo} />
      </View>
      <View style={styles.title}><Text style={{ fontSize: 24, color: '#4B73FF'}}>SanberDaily</Text></View>
      <View style={styles.content}>
        <View style={styles.formContainer}>
            <View style={styles.inputContainer}>
              <View>
                <Text style={styles.labelText}>Email</Text>
                <TextInput
                  style={styles.textBox}
                  placeholder='email@domain.com'
                  onChangeText={email => this.setState({ email })}
                />
              </View>
            </View>
            <View style={styles.inputContainer}>
              <View>
                <Text style={styles.labelText}>Password</Text>
                <TextInput
                  style={styles.textBox}
                  placeholder='Masukkan Password'
                  onChangeText={password => this.setState({ password })}
                  secureTextEntry={true}
                />
              </View>
            </View>
          </View>
          <TouchableOpacity onPress={() => this.loginHandler()}>
              <View style={styles.button1}>
                  <Text style={styles.text}>Login</Text>
              </View>
          </TouchableOpacity>
          <View style={styles.buttonArr}>
              <Text style={{ fontSize: 16, marginTop: 10, marginBottom: 10 }}></Text>
          </View>
          <TouchableOpacity onPress={() => this.onSignUpPress()}>
              <View style={styles.button2}>
                  <Text style={styles.text}>Register</Text>
              </View>
          </TouchableOpacity>
      </View>                
    </View>
    )
  }
};


const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white',
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  header: {
      marginTop: 50,
      height: 120,
      backgroundColor: 'white',
      elevation: 3,
      paddingHorizontal: 15,
      flexDirection: 'row',
      alignItems: 'center'
  },
  logo: { 
      width: 360, 
      height: 55, 
      marginLeft: 50 
  },
  title: {
      marginTop: 50,
      backgroundColor: 'white',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      height: 55
  },
  content: {
      height: 600,
      alignItems: 'center',
      padding: 20,
      marginTop:50
  },
  textKeterangan:{ 
      fontSize:16,
      textAlign: "left" 
  },
  textBox: {
      width: 300,
      height: 40,
      marginLeft: 25,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: 'grey',
      marginBottom: 20
  },
  button1: {
      
      width: 110,
      height: 50,
      borderRadius: 5,
      backgroundColor: '#B1FFFA'
  },
  button2: {
      
      width: 110,
      height: 50,
      borderRadius: 5,
      backgroundColor: '#FFB1B1'
  },
  buttonArr: {
      width: 300,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
  },
  text: {
      textAlign: 'center',
      marginTop: 8,
      color: 'white',
      fontWeight: 'bold',
      fontSize: 24
  }
});