import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    
    
  }


  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
    <Text style={styles.headerText}>{this.props.route.params.email}</Text>
            </Text>

            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>tempat total harga</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        <View style={{flex:1}}>
          <FlatList
            data={data.produk}
            renderItem={(produk) => <ListItem data={produk.item} function={this.updatePrice.bind(this)} />}
            keyExtractor={(item) => item.id}
            numColumns={2}
          />
        </View>


      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  itemContainer: {
    width: DEVICE.width * 0.44,
    backgroundColor: 'white',
    padding: 10,
    margin: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
},
itemImage: {
    width: 100,
    height: 100,
    alignItems: 'center'
},
itemName: {
  flex: 2,
  justifyContent: 'center'
},
itemPrice: {
    fontSize: 18,
    color: 'blue',
    textAlign: 'center'
},
itemStock: {
  flexDirection: 'row',
  justifyContent: 'center'
},
itemButton: {
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'center'
},
buttonText: {
  

}

})
